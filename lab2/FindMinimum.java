public class FindMinimum {

	public static void main(String[] args){
		int value1 = Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int value3 = Integer.parseInt(args[2]);
        
        int result;

        // Check first two parameter
        boolean firstCondition = value1 > value2;

        // get small one from first two
        result = firstCondition ? value2 : value1;

        // check small int and value3
        boolean secondCondition = result > value3;
        
        //get small one
        result = secondCondition ? value3 : result;

        System.out.println(result);

	}
}
