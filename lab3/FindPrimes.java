public class FindPrimes {

	public static void main(String[] args) {
        int max = Integer.parseInt(args[0]);
        boolean first = true;
        for(int x=0;x<max;x++){
            if(isPrime(x)){
                if(!first)
                    System.out.print(",");
                System.out.print(x);
                first = false;
            }
        }
        System.out.println("");
	}


    public static boolean isPrime(int number){
        int divisor = 2;
        boolean isPrime = true;

        while (divisor < number && isPrime){
            if(number % divisor == 0)
                isPrime = false;
            divisor++;
        }

        return isPrime;
    }
	
	
}
