import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		int guess; //Read the user input
		int attemp = 0;
		do{
			System.out.print("Type -1 to quit or give me as guess: ");
			guess = reader.nextInt();
			attemp++;
			if(guess == -1){
				System.out.println("Sorry, the number was " + number);
			}else if (number == guess){
				System.out.println("Congratulations! You won after "+ attemp + " attempts!");
			}else if(guess < number){
				System.out.println("Mine is greater than your guess.");
				System.out.println("Sorry");				
			}else {
				System.out.println("Mine is less than your guess.");
				System.out.println("Sorry");				
			}
		}while(number != guess && guess != -1);
		
		
		reader.close(); //Close the resource before exiting
	}
	
	
}
