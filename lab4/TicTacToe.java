import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		int moveCount = 0;
		int currentPlayer = 0;
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		printBoard(board);


		while(moveCount < 9){
			int row;
			int col;
			boolean correct;
			do{
				correct = true;
				row = -1;
				col = -1;
				System.out.print("Player " +(currentPlayer + 1) + " enter row number:");
				row = reader.nextInt();
				System.out.print("Player " +(currentPlayer + 1) + " enter column number:");
				col = reader.nextInt();

				// row and column must be in 0 and 3
				if(correct && (row > 3) || (col > 3) || (col < 0) || (row < 0)){
					System.out.println("Column and row must in 0 between 3 ");
					correct = false;
				}

				if(correct && board[row - 1][col - 1] != ' '){
					System.out.println("This selection is not empty");
					correct = false;
				}
			}while(!correct);

			//normalize col and row
			row--;
			col--;

			if(currentPlayer == 0){
				board[row ][col] = 'X';
			}else{
				board[row][col] = 'O';
			}
			printBoard(board);			
			//check is there any winner
			if(checkBoard(board,row,col)){
				System.out.print("Player " +(currentPlayer + 1) + " Win !");
				break;
			}
			currentPlayer = (currentPlayer + 1) % 2;
			moveCount++;
		}

		if(moveCount == 9){
			System.out.println("DRAW !");
		}

		reader.close();
	}

	public static boolean checkBoard(char[][] board,int row , int col){
		char cur = board[row][col];

		//check for row
		for(int x=0;x<3;x++){
			if(board[x][col] != cur){
				break;
			}
			if(x == 2) return true;
		}

		//check for col
		for(int x=0;x<3;x++){
			if(board[row][x] != cur){
				break;
			}
			if(x == 2) return true;
		}

		//check for cross 1
		if(col == row){
			for(int x=0;x<3;x++){
				if(board[x][x] != cur){
					break;
				}
				if(x==2) return true;
			}
		}
		
		// check for cross 2
		if(2-col == row){
			for(int x=0;x<3;x++){
				if(board[x][2-x] != cur){
					break;
				}
				if(x==2) return true;
			}
		}
		return false;
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}
