public class Rectangle{
    int sideA;
    int sideB;
    Point topLeft;

    public Rectangle(int sideA, int sideB, Point topLeft){
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }

    public int area(){
        return sideA * sideB;
    }

    public int perimeter(){
       return (sideA + sideB) * 2;
    }

    public Point[] corners(){ 

        return new Point[]{
            topLeft,
            new Point(topLeft.xCoord + sideA, topLeft.yCoord),
            new Point(topLeft.xCoord,topLeft.yCoord - sideB),
            new Point(topLeft.xCoord + sideA, topLeft.yCoord- sideB)
        };
        
    } 
}