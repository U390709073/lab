public class Main {

    public static void main(String[] args) {
        System.out.println("Testing Rectangle...");

        Rectangle myRectangle;

        myRectangle = new Rectangle(6, 8, new Point(0,0));	

        System.out.println("area=" + myRectangle.area());
        
        System.out.println("perimeter=" +  myRectangle.perimeter());

        System.out.println("corners:");

        for (Point p : myRectangle.corners()) {
            System.out.println("(" +  p.xCoord + "," + p.yCoord + ")");
        }


        System.out.println("Testing Circle...");

        Circle myCircle;
        
        myCircle = new Circle(4,new Point(10,10));
        
        myCircle.radius = 10;

        System.out.println("area=" + myCircle.area());
        
        System.out.println("perimeter=" +  myCircle.perimeter());

        Circle myCircleW = new Circle(4,new Point(17,10));

        System.out.println("intersect=" +  myCircle.intersect(myCircleW));

        Circle myCircleX = new Circle(4,new Point(25,10));

        System.out.println("intersect2=" +  myCircle.intersect(myCircleX));

	}

}
