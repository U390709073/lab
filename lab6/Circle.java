public class Circle {
	int radius;
    Point center;

	public Circle(int radius,Point center) {
		this.radius = radius;
		this.center = center;
    }
    
	public double area() {
		return radius * radius * Math.PI;
    }
    
	public double perimeter() {
		return Math.PI * radius * 2;
    }
    
    public boolean intersect(Circle circle) {
    	double distance = Math.sqrt(Math.pow(center.xCoord - circle.center.xCoord, 2) + Math.pow(center.yCoord - circle.center.yCoord, 2));
        return (circle.radius + radius) > distance;   
    }
    
}